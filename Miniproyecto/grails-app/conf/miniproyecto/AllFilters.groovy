package miniproyecto

class AllFilters {

    def filters = {
        filterUser(controller:'user', action:'*', actionExclude: 'index') {
            before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }
		filterRegular(controller:'regular', action:'*', actionExclude: 'index') {
			before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
		filterForum(controller:'forum', action:'*', actionExclude: 'index') {
			before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
		filterAdmin(controller:'admin', action:'*', actionExclude: 'index') {
			before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
		filterPost(controller:'post', action:'*', actionExclude: 'index') {
			before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
		filterFile(controller:'file', action:'*', actionExclude: 'index') {
			before = {
				if(session.authStatus != 'logged'){
					 redirect(action:"index")
					 print "redirected"
					 session.authStatus = 'logged'
					 }
			}
			after = { Map model ->

			}
			afterView = { Exception e ->

			}
		}
    }
}
