package miniproyecto

import java.util.Date;

class Post {

	String topic
	Date dateCreated = new Date()
	Date lastUpdate
	boolean isAllowed
	int rate
	String comments = [] 
	
	Regular regular
	Forum forum
	static belongsTo = [Regular, Forum]
	static hasMany = [files: File]
	
	static constraints = {
		topic(blank:false, size:3..50)
//		dateCreated()
		lastUpdate (nullable:false, validator: { val, obj -> val>= obj.dateCreated}) 
		isAllowed (blank:false)
		rate(min:0)
	}

	static mapping = {
		regular column: "owner_id"
		forum column: "fatherForum_id"
	}
	
	def beforeUpdate(){
		lastUpdate = new Date();
	}
	
//	def beforeDelete() {
//		files.removeAll(files)
//	}
	
//	def beforeInsert() {
//		if (dateCreated == NULL_DATE) {
//		   dateCreated = new Date()
//		}
//	}
}