package miniproyecto

class User {
	String name
	String lastName
	int age
	String username
	String password

    static constraints = {
		name(blank:false, size:3..50)
		lastName(blank:false, size:3..50)
		age(min:13)
		username(blank:false, unique:true)
		password(size:8..50, validator:{val-> 
			if(!val.any{Character.isUpperCase(it as Character)}) return "password.must.contain.upper.letter"
			if(!val.any{Character.isDigit(it as Character)}) return "password.must.contain.number"
			if(!val.any{Character.isLowerCase(it as Character)}) return "password.must.contain.lower.letter"
			}
		)
    }
	
	def String toString(){
		return "${name}-${lastName}-${username}"
	}
	
	static mapping = {
        tablePerHierarchy true
	}
}
