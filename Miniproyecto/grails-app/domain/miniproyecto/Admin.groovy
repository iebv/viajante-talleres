package miniproyecto

class Admin extends User {

	int level
	double rating

	static constraints = {
		level(range:1..5)
		rating(range:0..100)
	}
	
	static mapping = {
		sort level: "desc"
	}
	
	def String toString(){
		return "${name}-${lastName}-${username}"
	}
}
