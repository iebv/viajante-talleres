package miniproyecto

class Regular extends User {

	int postViews
	int strikesNumber
	int starsNumber
	
	static hasMany = [posts: Post]
	
    static constraints = {
		postViews()
		strikesNumber(range:0..3)
		starsNumber(range:0..3)
    }
	
	def beforeDelete() {
		posts.removeAll(posts)
	}
	
	def String toString(){
		return "${name}-${lastName}-${username}"
	}
}
