package miniproyecto

import java.awt.event.ItemEvent;

class File {

	String fileType
	byte[] content
	double size
	

	Post post
	static belongsTo = Post

	static constraints = {
		fileType(validator: {val ->
			def ch = []
			val.each{ch.add(it)}
			for (i in 0..ch.size()-1){
				if(ch[i] == "/" && i!=0 && i!=ch.size()-1) return true
			}
			return false
		})
		content()
		size(range:0..10)
	}
	
	static mapping = {
		post column: "post_belongs_id"
	}

//	def download(){
//	}

//	def share(){
//	}
}
