package miniproyecto

class Forum {

	String name
	Date dateCreated = new Date()
	String category
	
	static hasMany = [posts: Post]
	
    static constraints = {
		name(blank:false, size:3..50, unique:true)
		dateCreated()
		name(blank:false, size:3..15)
    }
	
//	def beforeInsert() {
//		if (dateCreated == NULL_DATE) {
//		   dateCreated = new Date()
//		}
//	}
//	
	def beforeDelete() {
			posts.removeAll(posts)
		}
	
	def String toString(){
		return "${name}-${dateCreated}-${category}"
	}
}