<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Grails</title>
		<style>
		.panel {
			background-color: #eee;
			float: left;
			margin: 5px;
			padding: 15px;
			width: 265px;
			height: 300px;
			border: 1px solid #fff;
			-moz-box-shadow: 0px 0px 1.25em #ccc;
			-webkit-box-shadow: 0px 0px 1.25em #ccc;
			box-shadow: 0px 0px 1.25em #ccc;
			-moz-border-radius: 0.6em;
			-webkit-border-radius: 0.6em;
			border-radius: 0.6em;
		}
		</style>
		<style type="text/css" media="screen">
			
			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
	<div class="nav" role="navigation">
		<ul>
			<%--	AQUI FALTA QUE CAMBIE LOS BOTONES CUANDO EL USUARIO HAYA INICIADO SESION	--%>
			<li><a class="login" href="user/login.gsp">Login</a></li>
		</ul>
	</div>
<%--		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>--%>
		<div class="panel">
		<h2>Bienvenido a la aplicación Forum 2.0</h2>
		<p>Consultar:</p>
		<p><a href="forum/index.gsp">Foros</a></p>
		<p><a href="regular/index.gsp">Usuarios Regulares</a></p>
		<p><a href="admin/index.gsp">Administradores</a></p>
		<p><a href="post/index.gsp">Entradas (Posts)</a></p>
		<p><a href="file/index.gsp">Archivos</a></p>
		</div>
		
		<div class="panel">
		<h2>Últimas entradas (Posts)</h2>
		</div>
		
		<div class="panel">
		<h2>Foros con mas entradas</h2>
		
		</div>
<%--		<div id="status" role="complementary" class="">--%>
<%--			<h1>Bienvenido a la aplicación Forum 2.0</h1>--%>
<%--			<ul>--%>
<%--				<li>Consultar: </li>--%>
<%--				<li>Foros: <g:meta name="app.version"/></li>--%>
<%--				<li>Usuarios: <g:meta name="app.grails.version"/></li>--%>
<%--				<li>Entradas(posts): ${GroovySystem.getVersion()}</li>--%>
<%--				<li>Archivos: ${System.getProperty('java.version')}</li>--%>
<%--			</ul>--%>
<%--		</div>--%>
<%--		<div id="page-body" role="main">--%>
<%--						<div id="controller-list" role="navigation">--%>
<%--				<h2>Available Controllers:</h2>--%>
<%--				<ul>--%>
<%--					<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--%>
<%--						<li class="controller"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>--%>
<%--					</g:each>--%>
<%--				</ul>--%>
<%--			</div>--%>
<%--		</div>--%>
	</body>
</html>
