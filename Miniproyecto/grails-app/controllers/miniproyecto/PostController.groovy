package miniproyecto

// la mayoria del código del controlador y las vistas fueron creadas 
// automaticamente con "print "generate-all miniproyecto.Post"

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PostController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def beforeInterceptor = {
		
		print ("Se va a ejecutar la accion: $actionName ")
	}
	
	def afterInterceptor = {
		print ("Se ha ejecutado la accion: $actionName ")
	}
	
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Post.list(params), model:[postInstanceCount: Post.count()]
    }

    def show(Post postInstance) {
        respond postInstance
    }

    def create() {
        respond new Post(params)
    }

    @Transactional
    def save(Post postInstance) {
        if (postInstance == null) {
            notFound()			
            return
        }
		
        if (postInstance.hasErrors()) {
            respond postInstance.errors, view:'create'
			print postInstance.errors
            return
        }
		
		postInstance.save (flush:true)
		
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'post.label', default: 'Post'), postInstance.id])
                redirect postInstance
            }
            '*' { respond postInstance, [status: CREATED] }
        }
    }

    def edit(Post postInstance) {
        respond postInstance
    }

    @Transactional
    def update(Post postInstance) {
        if (postInstance == null) {
            notFound()
            return
        }
		
        if (postInstance.hasErrors()) {
            respond postInstance.errors, view:'edit'
			print postInstance.errors
            return
        }
		
		postInstance.save flush:true
		
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Post.label', default: 'Post'), postInstance.id])
                redirect postInstance
            }
            '*'{ respond postInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Post postInstance) {

        if (postInstance == null) {
            notFound()
            return
        }

        postInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Post.label', default: 'Post'), postInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
		print ("Error: Post instance not found")
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'post.label', default: 'Post'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	def comment(){
		def postInstance = Post.findById(params.actualPost)
		if(postInstance){
			postInstance.comments << params.commentContent
			postInstance.save flush:true
			
//			if (postInstance.hasErrors()) {
//				respond postInstance.errors, view:'edit'
//				print postInstance.errors
//				return
//			}
			
			redirect(action:"index")
		}
		else{
			notFound()
			return
		}
	}

	def rate(){			
		def postInstance = Post.findById(params.actualPost)
		if(postInstance){
			postInstance.rate++
			postInstance.save flush:true
			
//			if (postInstance.hasErrors()) {
//				respond postInstance.errors, view:'edit'
//				print postInstance.errors
//				return
//			}
			
			redirect(action:"index")
		}
		else{
			notFound()
			return
		}
	}

	def share(){
		render "no se ha implementado esta funcionalidad"
	}
}