package miniproyecto

import static org.springframework.http.HttpStatus.*

import java.sql.Savepoint;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import grails.transaction.Transactional

@Transactional(readOnly = true)
class FileController{

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		if(File.list().size() == 0){
			params.max = Math.min(max ?: 10, 100)
			respond File.list(params), model:[fileInstanceCount: File.count()]
		}else render File.list(params)
	}

	def show(File fileInstance) {
		respond fileInstance
	}

	def create() {
		respond new File(params)
	}

	def edit(File fileInstance) {
		respond fileInstance
	}

	def upload() {
		def file = MultipartHttpServletRequest.getFile('myFile')
		if(file.getContentType()!="text/plain" || file.getContentType()!="image/png"){
			println "El tipo de archivo no es permitido"
			return
		}else{
			File archivo = new File()
			
			archivo.content=file.getBytes()
			archivo.size=file.getSize();
			
			if(file.getContentType()=="text/plain")
				archivo.fileType="txt"
			else
				archivo.fileType="png"
			
			save(archivo)
			
			println "El archivo se a subido exitosamente"
		}
	}
	
	def download(){
		def file = new File(params.actualFile)
		response.setContentType("application/octet-stream") //Atributo Content correspondientes a los bytes del archivo
		response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")
		response.outputStream << file.newInputStream()  
		println "El archivo se ha descargado exitosamente"
		return
	}
/**		Segunda Versi�n Para Descargar Archivos
	def download = {
		def file = File.get(params.id)
		response.setContentType( "application-xdownload") //Atributo Content correspondientes a los bytes del archivo
		response.setHeader("Content-Disposition", "attachment;filename=${file.downloadName}")
	    response.getOutputStream() << new ByteArrayInputStream( file.fileData.data )
	    println "El archivo se ha descargado exitosamente"
		return
	   }
**/	
	def share(){
		render "No se ha implementado esta funcionalidad"
	}
	
	@Transactional
	def save(File fileInstance) {
		if (fileInstance == null) {
			notFound()
			return
		}

		if (fileInstance.hasErrors()) {
			respond fileInstance.errors, view:'create'
			print fileInstance.errors
			return
		}

		fileInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'file.label', default: 'File'),
					fileInstance.id
				])
				redirect fileInstance
			}
			'*' { respond fileInstance, [status: CREATED] }
		}
	}
	
	@Transactional
	def delete(File fileInstance) {

		if (fileInstance == null) {
			notFound()
			return
		}

		fileInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'File.label', default: 'File'),
					fileInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'file.label', default: 'File'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}

	def beforeInterceptor = { println "Se va a ejecutar la accion ${actionName}" }

	def afterInterceptor = { println "Se ha ejecutado la accion ${actionName}" }
}
